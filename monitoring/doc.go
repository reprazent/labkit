/*
Package monitoring provides a monitoring endpoint for Go service processes.

- This endpoint will expose OpenMetrics at `/metrics`.

- Pprof service endpoints at `/debug/pprof`. More information about the pprof
	endpoints is available at https://golang.org/pkg/net/http/pprof/

- Support the gitlab_build_info metric for exposing application version information

*/
package monitoring
